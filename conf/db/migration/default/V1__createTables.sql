create table tasks (
  id bigserial not null,
  title varchar(20) not null,
  detail text,
  status varchar(1) not null,
  created_at timestamp not null default current_timestamp,
  update_at timestamp not null default current_timestamp
);
