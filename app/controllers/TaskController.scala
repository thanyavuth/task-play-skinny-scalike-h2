package controllers

import javax.inject.Inject

import models._
import play.api.i18n.MessagesApi
import play.api.libs.json._
import play.api.mvc._

class TaskController @Inject() (messages: MessagesApi) extends Controller with JsonReadWrites {

  private[this] val JsonContentType = "application/json; charset=utf-8"

  def index = Action {
    Ok(views.html.index())
  }

  def showTasks = Action {
    val tasks = Tasks.findTasks()
    Ok(Json.toJson(Map("data" -> tasks))).as(JsonContentType)
  }

  def showTask(task_id: Long) = Action {
    val tasks = Tasks.findTask(task_id)
    Ok(Json.toJson(Map("data" -> tasks))).as(JsonContentType)
  }

  def newTask = Action(parse.json) { request =>
    val taskData = request.body.as[Tasks]
    Tasks.create(taskData.title, taskData.detail, taskData.status)
    Created("Successful created")
  }

  def updateTask(task_id: Long) = Action(parse.json) { request =>
    val taskData = request.body.as[Tasks]
    Tasks.updateById(task_id).withAttributes(
      'title -> taskData.title,
      'detail -> taskData.detail,
      'status -> taskData.status
    )
    Ok("Successful updated")
  }

  def deleteTask(task_id: Long) = Action {
    Tasks.deleteById(task_id)
    Ok("Successful deleted")
  }

}
