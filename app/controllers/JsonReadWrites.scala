package controllers

import play.api.libs.json._
import play.api.libs.functional.syntax._
import models._
import org.joda.time.DateTime

trait JsonReadWrites {

  /*  Alternatively Writes */
  //  implicit val taskWrites = new Writes[Tasks] {
  //    def writes(task: Tasks) = Json.obj(
  //      "id" -> task.id,
  //      "title" -> task.title,
  //      "detail" -> task.detail,
  //      "status" -> task.status,
  //      "createdAt" -> task.createdAt,
  //      "updateAt" -> task.updateAt
  //    )
  //  }

  implicit val taskWrites: Writes[Tasks] = (
    (JsPath \ "id").writeNullable[Long] and
    (JsPath \ "title").write[String] and
    (JsPath \ "detail").write[String] and
    (JsPath \ "status").write[String] and
    (JsPath \ "createdAt").writeNullable[DateTime] and
    (JsPath \ "updateAt").writeNullable[DateTime]
  )(unlift(Tasks.unapply))

  implicit val taskRead: Reads[Tasks] = (
    (JsPath \ "id").readNullable[Long] and
    (JsPath \ "title").read[String] and
    (JsPath \ "detail").read[String] and
    (JsPath \ "status").read[String] and
    (JsPath \ "createdAt").readNullable[DateTime] and
    (JsPath \ "updateAt").readNullable[DateTime]
  )(Tasks.apply _)
}
