package models

import org.joda.time.DateTime

case class TasksForm(
  title: String,
  detail: String,
  status: String,
  createdAt: DateTime,
  updateAt: DateTime
)
