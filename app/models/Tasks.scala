package models

import scalikejdbc._
import skinny.orm._
import org.joda.time._

case class Tasks(
  id: Option[Long],
  title: String,
  detail: String,
  status: String,
  createdAt: Option[DateTime],
  updateAt: Option[DateTime]
)

object Tasks extends SkinnyCRUDMapper[Tasks] {
  override lazy val defaultAlias = createAlias("t")
  private[this] lazy val t = defaultAlias

  override def extract(rs: WrappedResultSet, rn: ResultName[Tasks]) = new Tasks(
    id = rs.longOpt(rn.id),
    title = rs.string(rn.title),
    detail = rs.string(rn.detail),
    status = rs.string(rn.status),
    createdAt = rs.jodaDateTimeOpt(rn.createdAt),
    updateAt = rs.jodaDateTimeOpt(rn.updateAt)
  )

  def create(title: String, detail: String, status: String): Long = {
    createWithNamedValues(
      column.title -> title,
      column.detail -> detail,
      column.status -> status
    )
  }

  def findTasks(): Seq[Tasks] = {
    Tasks.limit(100)
      .orderBy(t.createdAt.desc)
      .apply()
  }

  def findTask(task_id: Long): Option[Tasks] = {
    Tasks.findById(task_id)
  }
}
